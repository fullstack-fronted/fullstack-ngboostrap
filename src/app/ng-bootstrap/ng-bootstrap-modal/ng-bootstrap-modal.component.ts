import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ng-bootstrap-modal',
  templateUrl: './ng-bootstrap-modal.component.html',
  styleUrls: ['./ng-bootstrap-modal.component.scss']
})
export class NgBootstrapModalComponent implements OnInit {

  constructor(private modalService: NgbModal) {
  }

  ngOnInit() {
  }

  openBackDropCustomClass(content: HTMLElement) {
    this.modalService.open(content, {
      backdrop: 'static',
      backdropClass: 'light-blue-backdrop',
      centered: true,
      size: 'xl',
      keyboard: true,
      windowClass: 'dark-modal'
    });
  }
}
