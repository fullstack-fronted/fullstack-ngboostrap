import { Component, OnInit } from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ng-bootstrap-carousel',
  templateUrl: './ng-bootstrap-carousel.component.html',
  styleUrls: ['./ng-bootstrap-carousel.component.scss']
})
export class NgBootstrapCarouselComponent implements OnInit {
  images = [1, 2, 3, 4].map(() =>
    `https://picsum.photos/900/500?random&t=${Math.random()}`);
  constructor(config: NgbCarouselConfig) {
    config.interval = 10000; // Tiempo que durará para cambiar
    config.wrap = false; // para hacerlo circular
    config.keyboard = true; // para controlar con las flechas del teclado
    config.pauseOnHover = false; // para que se detenga cuando el mouse este encima
  }
  ngOnInit() {
  }
}
