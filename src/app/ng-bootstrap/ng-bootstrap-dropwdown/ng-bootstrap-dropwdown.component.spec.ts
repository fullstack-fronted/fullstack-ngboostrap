import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgBootstrapDropwdownComponent } from './ng-bootstrap-dropwdown.component';

describe('NgBootstrapDropwdownComponent', () => {
  let component: NgBootstrapDropwdownComponent;
  let fixture: ComponentFixture<NgBootstrapDropwdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgBootstrapDropwdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgBootstrapDropwdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
