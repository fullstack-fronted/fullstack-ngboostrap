import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrimengRoutingModule } from './primeng-routing.module';
import { PrimengMainComponent } from './primeng-main/primeng-main.component';
import { PrimengDataComponent } from './primeng-data/primeng-data.component';

@NgModule({
  declarations: [PrimengMainComponent, PrimengDataComponent],
  imports: [
    CommonModule,
    PrimengRoutingModule
  ]
})
export class PrimengModule { }
