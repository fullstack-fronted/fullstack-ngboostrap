import {Routes} from '@angular/router';
import {PrimengMainComponent} from './primeng-main/primeng-main.component';
import {PrimengDataComponent} from './primeng-data/primeng-data.component';

export const PRIMENG_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: PrimengMainComponent,
    children: [
      {
        path: 'data',
        component: PrimengDataComponent,
        data: {title: 'Laboratorio 3', content: 'This data was received from router and it is working!!!'},
      },
      {
        path: '',
        redirectTo: '/primeng/data',
        pathMatch: 'full'
      }
    ]
  }
];
