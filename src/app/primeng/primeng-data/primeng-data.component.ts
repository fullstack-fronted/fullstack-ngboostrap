import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-primeng-data',
  templateUrl: './primeng-data.component.html',
  styleUrls: ['./primeng-data.component.scss']
})
export class PrimengDataComponent implements OnInit {
  public data: { title: string, content: string };

  private readonly EMPTY = '';

  constructor(private _activatedRoute: ActivatedRoute) {
    this.data = {title: this.EMPTY, content: this.EMPTY};
  }

  ngOnInit() {
    this._activatedRoute.data.subscribe((data: { title: string, content: string }) => {
      this.data = data;
    });
  }

}
